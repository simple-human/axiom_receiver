# axiom_receiver

This library provides a derive macro for Axiom actor system. 
In Axiom all messages for an actor are handled by single function. 
To handle several messages you need a dispatcher boilerplate code. This derive macro generates the dispatcher for a number of messages.

## Usage
```rust
use std::sync::Arc;
use axiom::prelude::*;
use axiom_receiver::Receiver;
use serde::{Serialize, Deserialize};
 
#[derive(Serialize, Deserialize)]
struct TestMsg;
 
#[derive(Receiver)]
// A list of messages to implement, SystemMsg is optional
#[messages(TestMsg,SystemMsg)] 
// include optional handler for the rest of the messages. The default action for unknown msg is to panic.
#[messages_rest] 
struct Actor;
 
// the struct names are transformed to snake case
impl Actor {
     
  async fn receive_test_msg(mut self, _: Context, msg: Arc<TestMsg>) -> ActorResult<Self> {
    //...
    Ok(Status::done(self))
  }
   
  async fn receive_system_msg(mut self, _: Context, msg: Arc<SystemMsg>) -> ActorResult<Self> {
    //...
    Ok(Status::done(self))
  }
   
  async fn receive_rest(mut self, _: Context, msg: Message) -> ActorResult<Self> {
    // process unknown messages here
    Ok(Status::done(self))
  }
 
}
 
fn main() {
  let system = ActorSystem::create(ActorSystemConfig::default().thread_pool_size(2));
  // the receiver method was generated
  let aid = system.spawn().with( Actor, Actor::receiver ).unwrap();
  aid.send_new(TestMsg);
}
```