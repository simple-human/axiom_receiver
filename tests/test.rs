use axiom::prelude::*;
use axiom_receiver::Receiver;
use std::sync::Arc;
use std::sync::Mutex;

struct Probe(Option<String>);

async fn asserter(
    monitor_var: Arc<Mutex<Probe>>,
    _: Context,
    msg: Message,
) -> ActorResult<Arc<Mutex<Probe>>> {
    if let Some(msg) = msg.content_as::<SystemMsg>() {
        match &*msg {
            SystemMsg::Stopped { error, .. } => {
                monitor_var.lock().unwrap().0 = error.clone();
            }
            _ => (),
        }
    }

    Ok(Status::done(monitor_var.clone()))
}

#[test]
fn start() {
    let system = ActorSystem::create(ActorSystemConfig::default().thread_pool_size(2));
    let aid = system
        .spawn()
        .name("Test")
        .with(Actor { val: 1i32 }, Actor::receiver)
        .unwrap();

    let probe = Arc::new(Mutex::new(Probe(None)));
    let p = probe.clone();
    let monitor = system.spawn().with(p, asserter).unwrap();

    system.monitor(&monitor, &aid);

    aid.send_new("hi".to_string()).unwrap();
    aid.send_new(1u64).unwrap();
    aid.send_new(2i32).unwrap();

    system.await_shutdown(std::time::Duration::from_millis(100));

    probe.lock().unwrap().0.as_ref().map(|e| panic!("{}", e));
}

#[derive(Receiver)]
#[messages(String, u64, i32)]
#[messages(u64)]
#[messages_rest]
#[allow(dead_code)]
struct Actor<T> {
    val: T,
}

impl<T> Actor<T> {
    async fn receive_string(self, _: Context, msg: Arc<String>) -> ActorResult<Self> {
        assert_eq!(&"hi".to_string(), &*msg);
        Ok(Status::done(self))
    }

    async fn receive_u64(self, _: Context, msg: Arc<u64>) -> ActorResult<Self> {
        let msg = *msg;
        assert_eq!(1, msg);
        Ok(Status::done(self))
    }

    async fn receive_i32(self, _: Context, msg: Arc<i32>) -> ActorResult<Self> {
        let msg = *msg;
        assert_eq!(2, msg);
        Ok(Status::done(self))
    }

    async fn receive_rest(self, _: Context, _msg: Message) -> ActorResult<Self> {
        println!("Hey I receive smthing strange!!!");
        Ok(Status::done(self))
    }
}
