//! # axiom_receiver
//! This library provides a derive macro for Axiom actor system.
//! In Axiom all messages for an actor are handled by single function.
//! To handle several messages you need a dispatcher boilerplate code.
//! This derive macro generates the dispatcher for a number of messages.
//!
//! ## Usage

//! ```rust
//! use axiom::prelude::*;
//! use axiom_receiver::Receiver;
//! use serde::{Deserialize, Serialize};
//! use std::sync::Arc;
//!
//! #[derive(Serialize, Deserialize)]
//! struct TestMsg;
//!
//! #[derive(Receiver)]
//! // A list of messages to implement, SystemMsg is optional
//! #[messages(TestMsg, SystemMsg)]
//! // include optional handler for the rest of the messages. The default action for unknown msg is to panic.
//! #[messages_rest]
//! struct Actor;
//!
//! // the struct names are transformed to snake case
//! impl Actor {
//!     async fn receive_test_msg(mut self, _: Context, msg: Arc<TestMsg>) -> ActorResult<Self> {
//!         //...
//!         Ok(Status::done(self))
//!     }
//!
//!     async fn receive_system_msg(mut self, _: Context, msg: Arc<SystemMsg>) -> ActorResult<Self> {
//!         //...
//!         Ok(Status::done(self))
//!     }
//!
//!     async fn receive_rest(mut self, _: Context, msg: Message) -> ActorResult<Self> {
//!         // process unknown messages here
//!         Ok(Status::done(self))
//!     }
//! }
//!
//! fn main() {
//!     let system = ActorSystem::create(ActorSystemConfig::default().thread_pool_size(2));
//!     // the receiver method was generated
//!     let aid = system.spawn().with(Actor, Actor::receiver).unwrap();
//!     aid.send_new(TestMsg);
//! }
//! ```

extern crate heck;
extern crate proc_macro;
extern crate quote;
extern crate syn;
use heck::SnakeCase;
use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::parse::{Parse, ParseStream, Result};
use syn::punctuated::Punctuated;
use syn::{parenthesized, Ident, Token, TypePath};

#[derive(Clone)]
struct Msgs {
    msgs: Vec<TypePath>,
}

impl Parse for Msgs {
    fn parse(input: ParseStream) -> Result<Self> {
        let msgs;
        parenthesized!(msgs in input);
        let msgs = Punctuated::<TypePath, Token![,]>::parse_terminated(&msgs)?;
        let msgs = msgs.into_iter();

        Ok(Msgs {
            msgs: msgs.collect(),
        })
    }
}

#[proc_macro_derive(Receiver, attributes(messages, messages_rest))]
pub fn receiver(input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as syn::DeriveInput);
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let mut rest = false;
    let mut mesgs = vec![];

    let vis = input.vis;

    input
        .attrs
        .iter()
        .find(|atr| atr.path.segments[0].ident == "messages_rest")
        .map(|_| rest = true);

    input
        .attrs
        .iter()
        .filter(|atr| atr.path.segments[0].ident == "messages")
        .map(|atr| {
            let tokens = atr.tokens.clone().into();
            syn::parse2::<Msgs>(tokens)
        })
        .collect::<Result<Vec<Msgs>>>()
        .map(|mut msgs| msgs.iter_mut().for_each(|m| mesgs.append(&mut m.msgs)))
        .expect("Bad messages");

    let actor = input.ident;

    let fn_ids: Vec<Ident> = mesgs
        .clone()
        .iter()
        .map(|x| {
            let x = quote! {#x};
            format_ident!("receive_{}", x.to_string().to_snake_case()).into()
        })
        .collect();

    let mut rest_handler = quote! {
          {
            panic!("Bad Message!")
          }
    };

    if rest {
        rest_handler = quote! {
          {
            result = self.receive_rest(context, msg.clone()).await;
          }
        }
    }

    let out = quote! {

      impl #impl_generics #actor #ty_generics #where_clause {
        #vis async fn receiver(mut self, context: axiom::actors::Context, msg: axiom::message::Message) -> axiom::ActorResult<Self> {
          let mut result;
          #(
            if let Some(msg) = msg.content_as::<#mesgs>() {
              result = self.#fn_ids(context, msg.clone()).await;
            } else
          )*
          if let Some(_) = msg.content_as::<axiom::system::SystemMsg>() {
            result = Ok(Status::done(self))
          } else
          #rest_handler
          result
        }
      }
    };
    out.into()
}
